# WeatherApp

## Some notes

Application is live here - https://raulmetsva.github.io (https:// is necessary to get weather data based on users location ) - works on Chrome definitely and on Iphone looks ok as well.
Right now can't reload the app on live version. Will need to run it locally to test reloading.

App provides a 5 day forecast since openweathermap didn't have a free API for more than that.
In the next 24 h forecast section there is forecast for the next 4 times of day (didn't make sense showing already past periods temperatures - also not possible with openweathermap).
If the user doesn't reload the app checks for new data every 15 min. If user reloads then the saved data is shown from the browsers local storage and right after that the app checks for new data from the API.
App uses Google API as well to get a particular citys offset from the UTC time (to know what time it is there and to tailor the forecast for that city).



------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.0.1.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).
