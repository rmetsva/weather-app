import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import { SelectComponent } from './select/select.component';
import { WeatherComponent } from './weather/weather.component';
import { ConvertTempPipe } from './weather/convertTemp.pipe';

import { WeatherService } from './weather.service';

const appRoutes: Routes = [
  {path: '', component: SelectComponent},
  {path: 'weather', component: WeatherComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    SelectComponent,
    WeatherComponent,
    ConvertTempPipe
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    CommonModule,
    RouterModule.forRoot(appRoutes)
  ],
  providers: [
    WeatherService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
