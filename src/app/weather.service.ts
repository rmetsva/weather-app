import { Injectable } from '@angular/core';
import { Http, Headers } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/operator/map';

import { config } from '.././config/config';

const API_URL = config.apiUrl;
const API_KEY = config.apiKey;
const GOOGLE_API_URL = config.GoogleApiUrl;
const GOOGLE_API_KEY = config.GoogleApiKey;

@Injectable()
export class WeatherService {

  constructor(private http: Http) { }

  getCurrentWeatherByCityName(city: string): Observable<any> {
    return this.http.get(API_URL + 'weather?q=' + city + '&units=metric&APPID=' + API_KEY)
      .map(res => res.json());
  }

  getCurrentWeatherByCoordinates(latitude: number, longitude: number): Observable<any> {
    return this.http.get(API_URL + 'weather?lat=' + latitude + '&lon=' + longitude + '&units=metric&APPID=' + API_KEY)
      .map(res => res.json());
  }

  getForecastByCityName(city: string): Observable<any> {
    return this.http.get(API_URL + 'forecast?q=' + city + '&units=metric&APPID=' + API_KEY)
      .map(res => res.json());
  }

  getForecastByCoordinates(latitude: number, longitude: number): Observable<any> {
    return this.http.get(API_URL + 'forecast?lat=' + latitude + '&lon=' + longitude + '&units=metric&APPID=' + API_KEY)
      .map(res => res.json());
  }

  getTimezoneforCity(latitude: number, longitude: number): Observable<any> {
    const time = new Date();
    const timestamp = Math.floor(time.getTime() / 1000);
    return this.http.get(GOOGLE_API_URL + '?location=' + latitude + ',' + longitude + '&timestamp=' + timestamp + '&key=' + GOOGLE_API_KEY)
      .map(res => res.json());
  }
}
