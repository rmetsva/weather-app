import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
    name: 'convertTemp'
})
export class ConvertTempPipe implements PipeTransform {
    transform(value: number) {
        return Math.round(value * 1.8 + 32);
    }
}
