import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { WeatherService } from '../weather.service';

import { svgMapping } from '../../config/svgMapping';

@Component({
  selector: 'app-weather',
  templateUrl: './weather.component.html',
  styleUrls: ['./weather.component.styl']
})
export class WeatherComponent implements OnInit {

  currentCity: City = {
    name: '',
    currentDate: new Date(),
    currentTemperature: null,
    currentWeather: '',
    iconPath: '',
    forecastForDay: [],
    forecastForWeek: []
  };
  isFahrenheit: boolean;

  constructor(private weatherService: WeatherService, private router: Router) { }

  ngOnInit() {
    if (localStorage.getItem('weather')) {
      this.getCurrentCityData();
      if (localStorage.getItem('loaded') === 'true') {
        if (localStorage.getItem('timezoneOffset')) {
          this.refreshDataByCityName();
        } else {
          this.refreshDataByCoordinates();
        }
      }
      setInterval(() => {
        if (localStorage.getItem('timezoneOffset')) {
          this.refreshDataByCityName();
        } else {
          this.refreshDataByCoordinates();
        }
      }, 900000);
    } else {
      this.router.navigate(['/']);
    }
    localStorage.setItem('loaded', JSON.stringify(true));
  }

  getCurrentCityData() {
    this.currentCity.name = JSON.parse(localStorage.getItem('weather')).name;
    const d = new Date();
    const offset = this.getTimezoneOffsetHour();
    const utc = d.getTime() + d.getTimezoneOffset() * 6000;
    this.currentCity.currentDate = new Date(utc + (3600000 * offset));
    this.currentCity.currentTemperature = Math.round(JSON.parse(localStorage.getItem('weather')).main.temp);
    this.currentCity.currentWeather = JSON.parse(localStorage.getItem('weather')).weather[0].description;
    this.currentCity.iconPath = 'assets/svg/' + svgMapping[JSON.parse(localStorage.getItem('weather')).weather[0].main] + '.svg';
    this.getForecastsForDay();
    this.getForecastForWeek();
  }

  getForecastsForDay() {
    this.currentCity.forecastForDay = [];
    for (let i = 1; i < 8; i = i + 2) {
      const localHours = this.getLocalTimeForParticularForecast(i)[0];
      const forecast = this.getLocalTimeForParticularForecast(i)[1];
      if (localHours >= 6 && localHours <= 11) {
        this.currentCity.forecastForDay.push({ timeOfDay: 'Morning', temperature: Math.round(forecast.main.temp) });
      } else if (localHours >= 12 && localHours <= 17) {
        this.currentCity.forecastForDay.push({ timeOfDay: 'Day', temperature: Math.round(forecast.main.temp) });
      } else if (localHours >= 18 && localHours <= 23) {
        this.currentCity.forecastForDay.push({ timeOfDay: 'Evening', temperature: Math.round(forecast.main.temp) });
      } else {
        this.currentCity.forecastForDay.push({ timeOfDay: 'Night', temperature: Math.round(forecast.main.temp) });
      }
    }
  }

  getForecastForWeek() {
    const days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
    const hoursOfFirstForecast = this.getLocalTimeForParticularForecast(0)[0];
    let firstForecastToBePushedIndex;

    for (let i = 0; i < 8; i++) {
      if (i <= 4) {
        if (hoursOfFirstForecast >= (13 - i * 3) && hoursOfFirstForecast <= (15 - i * 3)) {
          firstForecastToBePushedIndex = i;
        }
      } else if (i === 5) {
        if (hoursOfFirstForecast === 22 || hoursOfFirstForecast === 23 || hoursOfFirstForecast === 0) {
          firstForecastToBePushedIndex = i;
        }
      } else if (i === 6) {
        if (hoursOfFirstForecast === 19 || hoursOfFirstForecast === 20 || hoursOfFirstForecast === 21) {
          firstForecastToBePushedIndex = i;
        }
      } else {
        if (hoursOfFirstForecast === 16 || hoursOfFirstForecast === 17 || hoursOfFirstForecast === 18) {
          firstForecastToBePushedIndex = i;
        }
      }
    }
    this.currentCity.forecastForWeek = [];
    for (let i = firstForecastToBePushedIndex; i < 40; i = i + 8) {
      const nextDayOfWeekIndex = this.getLocalTimeForParticularForecast(i)[2].getDay();
      const nextDayForecast = this.getLocalTimeForParticularForecast(i)[1];
      this.currentCity.forecastForWeek.push({ dayOfWeek: days[nextDayOfWeekIndex], temperature: Math.round(nextDayForecast.main.temp) });
    }

  }

  getLocalTimeForParticularForecast(i) {
    const forecast = JSON.parse(localStorage.getItem('forecast')).list[i];
    const timestamp = forecast.dt;
    const localDate = new Date((timestamp + this.getTimezoneOffsetHour() * 3600) * 1000);
    const localHours = localDate.getUTCHours();
    return [localHours, forecast, localDate];
  }

  getTimezoneOffsetHour() {
    if (localStorage.getItem('timezoneOffset')) {
      return JSON.parse(localStorage.getItem('timezoneOffset')) / 3600; // nr of hours offset of UTC time
    } else {
      const time = new Date();
      const timezoneOffset = time.getTimezoneOffset() / -60; // nr of hours offset of UTC time
      return timezoneOffset;
    }
  }

  refreshDataByCityName() {
    this.weatherService.getCurrentWeatherByCityName(this.currentCity.name)
      .subscribe(weather => {
        localStorage.setItem('weather', JSON.stringify(weather));
        this.weatherService.getForecastByCityName(this.currentCity.name)
          .subscribe(forecast => {
            localStorage.setItem('forecast', JSON.stringify(forecast));
            this.weatherService.getTimezoneforCity(forecast.city.coord.lat, forecast.city.coord.lon)
              .subscribe(timezone => {
                localStorage.setItem('timezoneOffset', timezone.rawOffset);
                this.getCurrentCityData();
              });
          });
      });
  }

  refreshDataByCoordinates() {
    navigator.geolocation.getCurrentPosition(loc => {
      this.weatherService.getCurrentWeatherByCoordinates(loc.coords.latitude, loc.coords.longitude)
        .subscribe(weather => {
          localStorage.setItem('weather', JSON.stringify(weather));
          this.weatherService.getForecastByCoordinates(loc.coords.latitude, loc.coords.longitude)
            .subscribe(forecast => {
              localStorage.setItem('forecast', JSON.stringify(forecast));
              this.getCurrentCityData();
            });
        });
    });
  }

}

interface City {
  name: string;
  currentDate: Date;
  currentTemperature: number;
  currentWeather: string;
  iconPath: string;
  forecastForDay: any[];
  forecastForWeek: any[];
}

