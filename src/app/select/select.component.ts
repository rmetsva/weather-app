import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { WeatherService } from '../weather.service';

@Component({
  selector: 'app-select',
  templateUrl: './select.component.html',
  styleUrls: ['./select.component.styl']
})
export class SelectComponent implements OnInit {

  selectedCity: string;
  longitude: number;
  latitude: number;

  constructor(private weatherService: WeatherService, private router: Router) { }

  ngOnInit() {
    localStorage.clear();
  }

  getWeatherByCityName() {
    this.weatherService.getCurrentWeatherByCityName(this.selectedCity)
      .subscribe(weather => {
        localStorage.setItem('weather', JSON.stringify(weather));
        this.weatherService.getForecastByCityName(this.selectedCity)
          .subscribe(forecast => {
            localStorage.setItem('forecast', JSON.stringify(forecast));
            this.weatherService.getTimezoneforCity(forecast.city.coord.lat, forecast.city.coord.lon)
              .subscribe(timezone => {
                localStorage.setItem('timezoneOffset', timezone.rawOffset);
                localStorage.setItem('loaded', JSON.stringify(false));
                this.router.navigate(['/weather']);
              });
          });
      });
  }

  getWeatherByCoords() {
    navigator.geolocation.getCurrentPosition(loc => {
      this.latitude = loc.coords.latitude;
      this.longitude = loc.coords.longitude;
      this.weatherService.getCurrentWeatherByCoordinates(this.latitude, this.longitude)
        .subscribe(weather => {
          localStorage.setItem('weather', JSON.stringify(weather));
          this.weatherService.getForecastByCoordinates(this.latitude, this.longitude)
            .subscribe(forecast => {
              localStorage.setItem('forecast', JSON.stringify(forecast));
              localStorage.setItem('loaded', JSON.stringify(false));
              this.router.navigate(['/weather']);
            });
        });
    });
  }

}
