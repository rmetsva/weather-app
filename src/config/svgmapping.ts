export const svgMapping = {
    'Rain': 'wi-rain',
    'Snow': 'wi-snow',
    'Mist': 'wi-fog',
    'Thunderstorm': 'wi-storm-showers',
    'Clear': 'wi-day-sunny',
    'Clouds': 'wi-cloud',
    'Drizzle': 'wi-rain',
    'Haze': 'wi-day-haze'
};
